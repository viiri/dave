/*
 * Copyright (C) 2011 Sergei Kolzun
 *
 * This program is free software: you can redistribute  it and/or modify
 * it under the  terms of the GNU General  Public License as published
 * by the Free  Software Foundation; either version 3  of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
 * MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/types.h"
#include "include/error.h"
#include "include/file.h"
#include "include/huff.h"

#include "src/file.c"
#include "src/huff.c"

int main(int argc, char *argv[])
{
  FILE *fp;
  uint8 *unpacked;
  int retval;

  retval = unHUFF(argv[1], &unpacked);
  if(retval > 0) {
    fp = fopen("output", "wb");
    if(fp == NULL) {
      fprintf(stderr, "Can't open output file!\n");
      free(unpacked);
      return -1;
    }
    fwrite(unpacked, 1, retval, fp);
    fclose(fp);
    free(unpacked);
  }
  return 0;
}
