/*
 * Copyright (C) 2011 Sergei Kolzun
 *
 * This program is free software: you can redistribute  it and/or modify
 * it under the  terms of the GNU General  Public License as published
 * by the Free  Software Foundation; either version 3  of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
 * MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

uint16 fgetw(FILE *fp)
{
  uint8 data[2];
  data[0] = fgetc(fp);
  data[1] = fgetc(fp);
  return (data[1] << 8) | data[0];
}

uint32 fgetl(FILE *fp)
{
  uint8 data[4];
  data[0] = fgetc(fp);
  data[1] = fgetc(fp);
  data[2] = fgetc(fp);
  data[3] = fgetc(fp);
  return (data[3] << 24) | (data[2] << 16) | (data[1] << 8) | data[0];
}
