/*
 * Copyright (C) 2011 Sergei Kolzun
 *
 * This program is free software: you can redistribute  it and/or modify
 * it under the  terms of the GNU General  Public License as published
 * by the Free  Software Foundation; either version 3  of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
 * MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

int unHUFF(char *filename, uint8 **output)
{
  FILE *fp;
  char signature[4];
  uint32 unpacked_size;
  uint32 i = 0;
  huffman_tree code_table[255]; 
  huffman_node node;
  uint8 byte;
  uint8 mask;

  fp = fopen(filename, "rb");
  if (!fp) {
    printf("unHUFF(): Cannot open %s.\n", filename);
    return ERROR_FILE_NOT_FOUND;
  }

  fread(signature, 4, 1, fp);
  if (strncmp(signature, "HUFF", 4) != 0) {
    printf("unHUFF(): Tried to load a file that isn't HUFF!\n");
    fclose(fp);
    return ERROR_INVALID_FILE;
  }

  unpacked_size = fgetl(fp);
  *output = (uint8 *)malloc(unpacked_size);
  if (*output == NULL) {
    printf("unHUFF(): Not enough memory!\n");
    fclose (fp);
    return ERROR_NOT_ENOUGH_MEMORY;
  }

  fread(&code_table, sizeof(huffman_tree), 255, fp);

  node = ROOT;
  while(i < unpacked_size)
  {
    byte = fgetc(fp);
    mask = 1;
    while(i < unpacked_size && mask)
    {
      node = (byte & mask) ? code_table[node.value].right : code_table[node.value].left;
      mask <<= 1;
      if(!node.isLeaf) {
        (*output)[i++] = node.value;
        node = ROOT;
      }
    }
  }

  fclose(fp);
  return unpacked_size;
}
