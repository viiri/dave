/*
 * Copyright (C) 2011 Sergei Kolzun
 *
 * This program is free software: you can redistribute  it and/or modify
 * it under the  terms of the GNU General  Public License as published
 * by the Free  Software Foundation; either version 3  of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
 * MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

int unRLEW(char *filename, uint16 **output)
{
  FILE *fp;
  uint32 unpacked_size;
  uint32 i = 0;
  uint16 word;
  uint16 count;

  fp = fopen(filename, "rb");
  if (!fp) {
    printf("unRLEW(): Cannot open %s.\n", filename);
    return ERROR_FILE_NOT_FOUND;
  }

  unpacked_size = fgetl(fp);
  *output = (uint16 *)malloc(unpacked_size);
  if (*output == NULL) {
    printf("unRLEW(): Not enough memory!\n");
    fclose (fp);
    return ERROR_NOT_ENOUGH_MEMORY;
  }

  unpacked_size >>= 1;
  while(!feof(fp) && i < unpacked_size)
  {
    word = fgetw(fp);
    if (word == 0xFEFE) {
      count = fgetw(fp);
      word = fgetw(fp);
      while(count--) (*output)[i++] = word;
    } else
      (*output)[i++] = word;
  }

  fclose(fp);

  if (i < unpacked_size) {
    printf("unRLEW(): Less data exists than specified in header!\n");
    return ERROR_INVALID_FILE;
  }

  return unpacked_size;
}

int unRLEB(char *filename, uint8 **output)
{
  FILE *fp;
  uint32 unpacked_size;
  uint32 i = 0;
  uint8 byte;
  uint8 count;

  fp = fopen(filename, "rb");
  if (!fp) {
    printf("unRLEB(): Cannot open %s.\n", filename);
    return ERROR_FILE_NOT_FOUND;
  }

  unpacked_size = fgetl(fp);
  *output = (uint8 *)malloc(unpacked_size);
  if (*output == NULL) {
    printf("unRLEB(): Not enough memory!\n");
    fclose (fp);
    return ERROR_NOT_ENOUGH_MEMORY;
  }

  while(!feof(fp) && i < unpacked_size)
  {
    byte = fgetc(fp);
    if (byte == 0xFE) {
      count = fgetc(fp);
      byte = fgetc(fp);
      while(count--) (*output)[i++] = byte;
    } else
      (*output)[i++] = byte;
  }

  fclose(fp);

  if (i < unpacked_size) {
    printf("unRLEB(): Less data exists than specified in header!\n");
    return ERROR_INVALID_FILE;
  }

  return unpacked_size;
}
