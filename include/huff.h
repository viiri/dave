typedef struct
{
  uint8 value;
  uint8 isLeaf;
} huffman_node;

typedef struct
{
  huffman_node left;
  huffman_node right;
} huffman_tree;

#define ROOT (huffman_node){254, 1};

int unHUFF(char *filename, uint8 **output);
